const Path = require("path");
const vuePlugin = require("@vitejs/plugin-vue");

const { defineConfig } = require("vite");

/**
 * https://vitejs.dev/config
 */
const config = defineConfig({
  root: Path.join(__dirname, "src", "browser"),
  publicDir: "public",
  server: {
    host: "0.0.0.0",
  },
  open: false,
  build: {
    outDir: Path.join(__dirname, "build", "browser"),
    emptyOutDir: true,
  },
  plugins: [vuePlugin()],
  resolve: {
    alias: {
      "@": Path.join(__dirname, "src", "browser"),
    },
  },
});

module.exports = config;
