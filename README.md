# Electron Vue Template

A simple starter template for a **Vue3** + **Electron** TypeScript based application, including **ViteJS** and **Electron Builder**.

> Based off [Deluze's template](https://github.com/Deluze/electron-vue-template)

## About

This template utilizes [ViteJS](https://vitejs.dev) for building and serving your (Vue powered) front-end process, it provides Hot Reloads (HMR) to make development fast and easy ⚡

Building the Electron (main) process is done with [Electron Builder](https://www.electron.build/), which makes your application easily distributable and supports cross-platform compilation 😎

## Getting started

Click the green **Use this template** button on top of the repository, and clone your own newly created repository.

**Or..**

Clone this repository: `git clone git@gitlab.com:thebartyparty/electron-vite-vue-boilerplate.git`

### Install dependencies ⏬

```bash
npm install
```

### Start developing ⚒️

```bash
npm run dev
```

## Additional Commands

```bash
npm run dev # starts application with hot reload
npm run build # builds application

# OR

npm run build:win # uses windows as build target
npm run build:mac # uses mac as build target
npm run build:linux # uses linux as build target
```

Optional configuration options can be found in the [Electron Builder CLI docs](https://www.electron.build/cli.html).

## Project Structure

```bash
- root
  - scripts/ # all the scripts used to build or serve your application, change as you like.
  - src/
    - main/ # Main thread - Backend/Node.js
    - browser/ # Browser thread - Frontend/Vue.js
  - electron-builder.config.json # Builder configuration file
  - vite.config.js # Vite configuration file
```

## Using static files

If you have any files that you want to copy over to the app directory after installation, you will need to add those files in your `src/main/static` directory.

#### Referencing static files from your main process

```js
/* Assumes src/main/static/yourFile.txt exists */

const { app } = require("electron");
const FileSystem = require("fs");
const Path = require("path");

const path = Path.join(app.getAppPath(), "static", "yourFile.txt");
const contents = FileSystem.readFileSync(path);
```
